<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    public $timestamps = false;

    protected $fillable = ['title','first_name', 'last_name', 'position'];


    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
