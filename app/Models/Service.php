<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $timestamps = false;

    protected $fillable = ['title', 'price','last_price','percent','stock','img'];

    public function catalogs()
    {
        return $this->belongsTo(Catalog::class);
    }
    public function masters()
    {
        return $this->belongsTo(Master::class);
    }
}
