<?php

use Illuminate\Database\Seeder;

class CatalogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Catalog::query()->truncate();

        factory(\App\Models\Catalog::class, 50)->create();
    }
}
