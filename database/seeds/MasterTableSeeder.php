<?php

use Illuminate\Database\Seeder;

class MasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Master::query()->truncate();

        factory(\App\Models\Master::class, 50)->create();
    }
}
