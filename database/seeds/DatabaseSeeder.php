<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CatalogTableSeeder::class);
        $this->call(MasterTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
    }
}
