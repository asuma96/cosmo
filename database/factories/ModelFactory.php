<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Models\Catalog::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->name,
    ];
});
$factory->define(App\Models\Service::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->name,
        'price' => $faker->randomDigitNotNull,
        'last_price' => $faker->randomDigitNotNull,
        'percent' => $faker->randomDigitNotNull,
        'catalog_id'=>$faker->randomDigitNotNull,
        'stock'=>$faker->boolean,
    ];
});
$factory->define(App\Models\Master::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->name,
        'first_name' => $faker->lastName,
        'last_name' => $faker->firstName,
        'position' => $faker->word,
    ];
});
