<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <span class="black-color">PROF</span><span class="red-color">COSMO</span>
            </a>
            <button type="button" class="btn btn-link navbar-toggle navbar-button" data-toggle="modal" data-target="#main-modal">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
    </div>
</nav>