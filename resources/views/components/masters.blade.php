<div class="container-fluid masters" id="masters">
    <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-3 text-center">
            <img src="/img/team1.png" class="img-fluid" alt="Responsive image">
            <p>Екатерина Попова<br>(Администратор)</p>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 text-center">
            <img src="/img/team2.png" class="img-fluid" alt="Responsive image">
            <p>Жанна Сафонова<br>(Мастер Маникюра)</p>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 text-center">
            <img src="/img/team3.png" class="img-fluid" alt="Responsive image">
            <p>Жиляева Татьяна<br>(Парикмахер)</p>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 text-center">
            <img src="/img/team4.png" class="img-fluid" alt="Responsive image">
            <p>Акимочкина Оксана<br>(Администратор)</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
            <a class="btn btn-default none-focus">Показать всех мастеров</a>
        </div>
    </div>
</div>