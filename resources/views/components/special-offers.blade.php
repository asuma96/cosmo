<div class="container-fluid special-offers" id="special-offers">
    <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-3 text-center special-offer">
            <img src="/img/tabs1-img1.jpg" class="img-fluid" alt="Responsive image">
            <p>
                <span class="offer">Повседневная укладка<br>(средние волосы)</span><br>
                <span class="old-price">500 р.</span><br>
                <span class="new-price">400 р.</span>
            </p>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 text-center special-offer">
            <img src="/img/tabs1-img2.jpg" class="img-fluid" alt="Responsive image">
            <p>
                <span class="offer">SPA Экранирование волос</span><br>
                <span class="old-price">850 р.</span><br>
                <span class="new-price">600 р.</span>
            </p>
        </div>
        <div class="clearfix visible-xs-block"></div>
        <div class="col-xs-6 col-sm-4 col-md-3 text-center special-offer">
            <img src="/img/tabs1-img3.jpg" class="img-fluid" alt="Responsive image">
            <p>
                <span class="offer">Женская Стрижка<br>(любой длины)</span><br>
                <span class="old-price">1500 р.</span><br>
                <span class="new-price">750 р.</span>
            </p>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 text-center special-offer">
            <img src="/img/tabs1-img4.jpg" class="img-fluid" alt="Responsive image">
            <p>
                <span class="offer">Окрашивание<br>в один тон</span><br>
                <span class="old-price">1500 р.</span><br>
                <span class="new-price">1100 р.</span>
            </p>
        </div>
    </div>
</div>