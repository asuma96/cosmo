<div class="modal fade" id="main-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="main-modal-close" class="close none-focus" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="navbar-brand modal-brand">
                    <span class="white-color">PROF</span><span class="red-color">COSMO</span>
                </span>
            </div>
            <div class="modal-body">
                <ul class="links text-center font-bold">
                    <li><a class="main-link" href="#main-top">ГЛАВНАЯ</a></li>
                    <li><a class="main-link" href="#catalog">КАТАЛОГ</a></li>
                    <li><a class="main-link" href="#masters">МАСТЕРА</a></li>
                    <li><a class="main-link" href="#special-offers">АКЦИИ</a></li>
                    <li><a class="main-link" href="#contacts">КОНТАКТЫ</a></li>
                    <li><a class="main-link" href="#appointment">ОНЛАЙН ЗАПИСЬ</a></li>
                </ul>
            </div>
            <div class="modal-footer">
                <p class="social-media white-color text-center">Мы в соц. сетах :) <img class="arrow" src="/img/arrow.png" class="social-media-link"></p>

                <div class="social-media-links text-center">
                    <img src="/img/fb.png" class="social-media-link">
                    <img src="/img/instagram.png" class="social-media-link">
                    <img src="/img/vk.png" class="social-media-link">
                </div>
            </div>
        </div>
    </div>
</div>