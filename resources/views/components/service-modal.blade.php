<div class="modal fade" id="service-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close none-focus" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="page-header">
                    <h4 class="service-title">
                        <span class="service-main-title">Повседневная укладка</span><br>
                        <small class="service-sub-title">(средние волосы)</small>
                    </h4>
                </div>
            </div>
            <div class="container modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <img src="/img/tabs1-img3.jpg">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A atque blanditiis cumque distinctio doloribus, eos iure labore necessitatibus similique tempora ut velit. Dolore eaque error fugit in odio quae tempore!</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A atque blanditiis cumque distinctio doloribus, eos iure labore necessitatibus similique tempora ut velit. Dolore eaque error fugit in odio quae tempore!</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A atque blanditiis cumque distinctio doloribus, eos iure labore necessitatibus similique tempora ut velit. Dolore eaque error fugit in odio quae tempore!</p>
                        <p>
                            <span class="old-price">1500 р.</span><br>
                            <span class="new-price">1100 р.</span>
                        </p>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                {{--<div class="button-container col-xs-10 col-sm-6 col-md-4">--}}
                <a class="btn btn-primary">записаться</a>
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>