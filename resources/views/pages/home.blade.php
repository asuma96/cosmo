@extends('layouts.default')

@section('content')
    <div class="row">

        <div class="col-md-12 text-center">
            <h3 class="h3" id="main-top">
                Центр красоты №1 в Орле
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="main-page-slider owl-carousel owl-theme">
                <img src="/img/photo-slide1.png">
                <img src="/img/photo-slide2.png">
                <img src="/img/photo-slide3.png">
                <img src="/img/photo-slide4.png">
                <img src="/img/photo-slide5.png">
                <img src="/img/photo-slide6.png">
                <img src="/img/photo-slide7.png">
                <img src="/img/photo-slide8.png">
                <img src="/img/photo-slide9.png">
                <img src="/img/photo-slide10.png">

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis consequatur doloremque ducimus earum est expedita harum ipsum laboriosam neque nostrum quod reprehenderit sed soluta suscipit tempora tempore vitae, voluptatibus voluptatum.</p>
                <div class="input-group tel-input col-xs-12 col-sm-6 col-md-4">
                    <input type="text" class="form-control" placeholder="+7 (***) *** - ** - **">
                    <span class="input-group-btn ">
                        <button class="btn font-bold white-color" type="button"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    @include('components.title-section', ['title'=> 'КАТАЛОГ'])
    @include('components.catalog')
    @include('components.title-section', ['title'=> 'НАШИ МАСТЕРА'])
    @include('components.masters')
    @include('components.title-section', ['title'=> 'НАШИ АКЦИИ'])
    @include('components.special-offers')
    @include('components.title-section', ['title'=> 'НАШИ КОНТАКТЫ'])
    @include('components.contacts')
    @include('pages.appointment')
@endsection