@extends('layouts.default')

@section('content')
    <div class="service-page">
        @include('components.title-section', ['title'=> 'ПАРИКМАХЕРСКИЙ ЗАЛ'])
        <div class="container-fluid catalog">
            <div class="row">
                <ul class="list-group font-bold">
                    <a href="" data-toggle="modal" data-target="#service-modal">
                        <li class="list-group-item">
                            <span class="service-name">Повседневная укладка (средние волосы)</span>
                            <span class="service-price ">
                                <span class="service-price-value">520 </span>
                                <span class="ruble"> Р</span>
                            </span>
                        </li>
                    </a>
                    <a href="">
                        <li class="list-group-item">
                            <span class="service-name">Стрижка мужская </span>
                            <span class="service-price ">
                                <span class="service-price-value">520 </span>
                                <span class="ruble"> Р</span>
                            </span>
                        </li>
                    </a>
                    <a href="">
                        <li class="list-group-item">
                            <span class="service-name">Стрижка мужская </span>
                            <span class="service-price ">
                                <span class="service-price-value">520 </span>
                                <span class="ruble"> Р</span>
                            </span>
                        </li>
                    </a>
                    <a href="">
                        <li class="list-group-item">
                            <span class="service-name">Стрижка мужская </span>
                            <span class="service-price ">
                                <span class="service-price-value">520 </span>
                                <span class="ruble"> Р</span>
                            </span>
                        </li>
                    </a>
                    <a href="">
                        <li class="list-group-item">
                            <span class="service-name">Стрижка мужская </span>
                            <span class="service-price ">
                                <span class="service-price-value">520 </span>
                                <span class="ruble"> Р</span>
                            </span>
                        </li>
                    </a>
                    <a href="">
                        <li class="list-group-item">
                            <span class="service-name">Стрижка мужская </span>
                            <span class="service-price ">
                                <span class="service-price-value">520 </span>
                                <span class="ruble"> Р</span>
                            </span>
                        </li>
                    </a>
                    <a href="">
                        <li class="list-group-item">
                            <span class="service-name">Стрижка мужская </span>
                            <span class="service-price ">
                                <span class="service-price-value">520 </span>
                                <span class="ruble"> Р</span>
                            </span>
                        </li>
                    </a>
                </ul>
            </div>
        </div>
        @include('components.service-modal')
    </div>
@endsection