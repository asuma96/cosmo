<div class="container-fluid appointment" id="appointment">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h3>Выберите услуги</h3>
        </div>
        <form>
            <div class="form-group">
                <div class="input-group search">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                    <input type="search" class="form-control" aria-label="Amount (to the nearest dollar)">
                </div>
            </div>
            <div class="form-group">
                <label for="service-category">Категория услуг</label>
                <select id="service-category" class="form-control">
                    <option>Женский зал</option>
                    <option>Женский зал</option>
                    <option>Женский зал</option>
                    <option>Женский зал</option>
                    <option>Женский зал</option>
                </select>
            </div>
            <div class="form-group">
                <label for="service">услуги</label>
                <select id="service" class="form-control">
                    <option>Выбрать услуги</option>
                    <option>Выбрать услуги</option>
                    <option>Выбрать услуги</option>
                    <option>Выбрать услуги</option>
                    <option>Выбрать услуги</option>
                </select>
            </div>
            <div class="form-group">
                <p>выбранные услуги</p>
                <div class="input-group col-xs-12">
                    <output class="picked-service">
                        <span class="picked-service-name">some text some text some text some text some text some text</span>
                        <span class="picked-service-control font-bold">
                            <span class="picked-service-price"><span>1500</span> <span>Р</span></span>
                            <a class="remove" type="button"><span aria-hidden="true">&times;</span></a>
                        </span>
                    </output>
                </div>
            </div>
            <input type="submit" class="btn btn-default col-xs-12" value="Далее">
        </form>
    </div>
</div>