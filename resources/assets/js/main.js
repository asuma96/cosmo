$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        items:1,
        dots: true,
        dotsEach: true,
        loop: true,
        nav:true,
    });
    $(".owl-prev, .owl-next").empty();
    $(".owl-prev").append('<span aria-hidden="true" class="glyphicon glyphicon-menu-left"></span>');
    $(".owl-next").append('<span aria-hidden="true" class="glyphicon glyphicon-menu-right"></span>');



    $('#main-modal .main-link').bind('click', function(e) {
        e.preventDefault();
        let target = $(this).attr("href");
        $('html, body').stop().animate({ scrollTop: $(target).offset().top - 120}, 1000, function() {
            location.hash = target;
        });
        return false;
    });

    $('.main-link').click(function(){
        $('#main-modal-close').trigger('click');
    });
});